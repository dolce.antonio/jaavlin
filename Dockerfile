FROM openjdk:8-jre-alpine
WORKDIR /app
COPY target/*shaded.jar /app/application.jar
EXPOSE 7000
CMD java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -XshowSettings:vm -jar application.jar
