/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.dulce.javalin.setup;

import io.javalin.Javalin;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nino
 */
public class HelloWorld {

    private static final Logger logger = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) {
        try {
            // /run/secrets/<
            String content = new String(Files.readAllBytes(Paths.get("/run/secrets/db_pass")));
            logger.info("password: " + content);
        } catch (IOException ex) {
            logger.error("could not print secret!", ex);
        }
        Javalin app = Javalin.create().start(7000);
        app.get("/", ctx -> ctx.json(new User("Donald", "Duck")));
    }

}
